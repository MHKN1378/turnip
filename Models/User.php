<?php namespace Models\User;

use Controllers\Auth\Auth;
use mysqli;

require './config/settings.php';

class User
{
    public function __construct($input_username)
    {
        $this->error = null;
        $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
        if($db->connect_error) {
            $this->error = "Connection error";
            return;
        }

        $data = $db->query("SELECT * FROM users WHERE username='".$input_username."'");

        if($val = $data->fetch_assoc()) {
            $this->fname = $val["fname"];
            $this->lname = $val["lname"];
            $this->username = $val["username"];
            $this->id = $val["id"];
            $this->avatar = $val["avatar"];
            $this->bio = $val["bio"];
            $this->password = $val["password"];
        }
        else
            $this->username = $input_username;

        $data = $db->query("SELECT * FROM followings WHERE user_id='".$this->id."'");

        while($val = $data->fetch_assoc())
        {
            array_push($this->followers, $val["follower_id"]);
        }

        $data = $db->query("SELECT * FROM followings WHERE follower_id='".$this->id."'");

        while($val = $data->fetch_assoc())
        {
            array_push($this->followings, $val["user_id"]);
        }

        $data = $db->query("SELECT * FROM posts WHERE user_id='".$this->id."'");

        while($val = $data->fetch_assoc())
        {
            array_push($this->posts, $val["id"]);
        }

        $db->close();
    }

    public function save()
    {
        $this->error = null;
        $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
        if($db->connect_error) {
            $this->error = "Connection error";
            return;
        }

        $data = $db->query("SELECT * FROM users WHERE id='".$this->id."'");

        if($data->num_rows == 0)
        {
            $db->query("INSERT INTO users (username, fname, lname, password, bio, avatar) VALUES ('"
                .$this->username."', '"
                .$this->fname."', '"
                .$this->lname."', '"
                .$this->password."', '"
                .$this->bio."', '"
                .$this->avatar."')");
            return;
        }
        $db->query("UPDATE users SET username = '".$this->username."'
                                , fname = '".$this->fname."'
                                , lname = '".$this->lname."'
                                , password = '".$this->password."'
                                , bio = '".$this->bio."'
                                , avatar  = '".$this->avatar."'
                                WHERE id = '".$this->id."'");
        $this->error = $db->error;
        return;
    }
    public function pass_check ($pass)
    {
        return ($pass === $this->password);
    }
    public function pass_assign ($pass)
    {
        $this->password = $pass;
    }

    public function avatar_upload($file)
    {
        $target_file = 'resources/avatars/';
        $target_file .= $this->id."_";
        $target_file .= strtotime('now');
        $target_file .= '.png';
        if (move_uploaded_file($file["tmp_name"], $target_file)) {
            $this->avatar = $target_file;
            return null;
        } else {
            return "Sorry, there was an error uploading your file";
        }
    }

    public function is_followed_by($user_id)
    {
        $this->error = null;
        $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
        if($db->connect_error) {
            $this->error = "Connection error";
            return null;
        }

        $data = $db->query("SELECT * FROM followings WHERE user_id='".$this->id."' AND follower_id='".$user_id."'");
        if($data->num_rows == 0)
            return false;
        else
            return true;
    }

    public function is_following($user_id)
    {
        $this->error = null;
        $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
        if($db->connect_error) {
            $this->error = "Connection error";
            return null;
        }

        $data = $db->query("SELECT * FROM followings WHERE follower_id='".$this->id."' AND user_id='".$user_id."'");
        if($data->num_rows == 0)
            return false;
        else
            return true;
    }

    public function is_liked($post_id)
    {
        $this->error = null;
        $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
        if($db->connect_error) {
            $this->error = "Connection error";
            return null;
        }

        $data = $db->query("SELECT * FROM likes WHERE user_id='".$this->id."' AND post_id='".$post_id."'");
        $this->error = $db->error;
        if($data->num_rows == 0)
            return false;
        else
            return true;
    }

    public function follow_toggle()
    {
        $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
        $data = $db->query("SELECT * FROM followings WHERE user_id='".$this->id."'"." AND follower_id='".Auth::user_id()."'");
        if($data->num_rows == 0)
            $db->query("INSERT INTO followings (follower_id, user_id) VALUES ('".Auth::user_id()."', '".$this->id."')");
        else
            $db->query("DELETE FROM followings WHERE user_id='".$this->id."'"." AND follower_id='".Auth::user_id()."'");
        $this->error = $db->error;
        $db->close();
    }

    public $fname;
    public $lname;
    public $id;
    public $avatar;
    private $password;
    public $username;
    public $bio;
    public $error;
    public $followings = [];
    public $followers = [];
    public $posts = [];
}