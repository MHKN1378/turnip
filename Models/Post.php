<?php namespace Models\Post;


use Controllers\Auth\Auth;
use mysqli;

class Post
{
    public function __construct($input_id = null)
    {
        if($input_id == null)
            return;

        $this->error = null;
        $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
        if($db->connect_error) {
            $this->error = "Connection error";
            return;
        }

        $data = $db->query("SELECT * FROM posts WHERE id='".$input_id."'");

        if($val = $data->fetch_assoc()) {
            $this->id = $val["id"];
            $this->picture = $val["picture"];
            $this->caption = $val["caption"];
            $this->user_id = $val["user_id"];
        }
        else
            $this->id = $input_id;

        $data = $db->query("SELECT * FROM comments WHERE post_id='".$this->id."'");

        while($val = $data->fetch_assoc())
        {
            array_push($this->comments, [$val["user_id"], $val['text']]);
        }

        $data = $db->query("SELECT * FROM likes WHERE post_id='".$this->id."'");

        while($val = $data->fetch_assoc())
        {
            array_push($this->likes, [$val["user_id"]]);
        }

        $db->close();
    }

    public function save()
    {
        $this->error = null;
        $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
        if($db->connect_error) {
            $this->error = "Connection error";
            return;
        }

        $data = $db->query("SELECT * FROM posts WHERE id='".$this->id."'");

        if($data->num_rows == 0)
        {
            $db->query("INSERT INTO posts (user_id, picture, caption) VALUES ('"
                .$this->user_id."', '"
                .$this->picture."', '"
                .$this->caption."')");
            $this->error = $db->error;
            return;
        }
        $db->query("UPDATE posts SET picture = '".$this->picture."'
                                , caption = '".$this->caption."'
                                WHERE id = '".$this->id."'");
        $this->error = $db->error;
        return;
    }

    public function picture_upload($file)
    {
        $target_file = 'resources/posts/';
        $target_file .= Auth::user_id()."_";
        $target_file .= strtotime('now');
        $target_file .= '.png';
        if (move_uploaded_file($file["tmp_name"], $target_file)) {
            $this->picture = $target_file;
            return null;
        } else {
            return "Sorry, there was an error uploading your file";
        }
    }

    public function like_toggle()
    {
        $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
        $data = $db->query("SELECT * FROM likes WHERE post_id='".$this->id."'"." AND user_id='".Auth::user_id()."'");
        if($data->num_rows == 0)
            $db->query("INSERT INTO likes (user_id, post_id) VALUES ('".Auth::user_id()."', '".$this->id."')");
        else
            $db->query("DELETE FROM likes WHERE post_id='".$this->id."'"." AND user_id='".Auth::user_id()."'");
        $this->error = $db->error;
        $db->close();
    }

    public function add_comment($text) {
        $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
        $sql = "INSERT INTO comments (user_id, post_id, text, c_timestamp) VALUES ( '".Auth::user_id()."', ";
        $sql .= "'".$this->id."', '".$text."', '".strtotime("now")."')";
        $db->query($sql);
        $this->error = $db->error;
        $db->close();
    }

    public $id;
    public $user_id;
    public $picture;
    public $caption;
    public $error;
    public $comments = [];
    public $likes = [];
}