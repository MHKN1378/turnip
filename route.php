<?php

use Controllers\Auth\Auth;
use Controllers\val\Validation;
use Models\Post\Post;
use Models\User\User;

require_once 'controllers/Auth.php';
require_once 'controllers/Validation.php';
require_once 'Models/Post.php';
require_once 'Models/User.php';

session_start();

$uri = empty($_GET['uri']) ? '/' : $_GET['uri'];

if(!Auth::logged_in()) {
    if ($uri == 'register')
        require_once 'views/register.php';
    else
        require_once 'views/login.php';
}
else {
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        switch ($uri) {
            case 'profile' :
                require_once 'views/edit_profile.php';
                break;
            case 'new_post' :
                require_once 'views/new_post.php';
                break;
            case 'comment' :
                $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
                $data = $db->query("SELECT * FROM posts WHERE id='".$_POST['post_id']."'");
                $row = $data->fetch_assoc();
                $post = new Post($row['id']);
                $db->close();

                $post->add_comment(Validation::test_input($_POST['text']));
                header("Location: http://".$server_domain."/post?id=".$post->id);
                break;
            default:
                header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
                break;
        }
    }
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
        switch ($uri) {
            case '/' :
                require_once 'views/home.php';
                break;
            case 'logout' :
                Auth::logout();
                header("Location: http://".$server_domain);
                break;
            case 'profile' :
                require_once 'views/profile.php';
                break;
            case 'page' :
                require_once 'views/page.php';
                break;
            case 'new_post' :
                require_once 'views/new_post.php';
                break;
            case 'like' :
                $post = new Post($_GET['id']);
                $post->like_toggle();
                $str = "Location: http://".$server_domain;
                switch($_GET['re'])
                {
                    case 'page' :
                        $str .= "/page?id=".$_GET['user']."#".$post->id;
                        break;
                    case 'followings' :
                        $str .= "/followings_posts#".$post->id;
                        break;
                    case 'post' :
                        $str .= "/post?id=".$_GET['id'];
                        break;
                    default :
                        $str .= "#".$post->id;
                }
                header($str);
                break;
            case 'follow' :
                if($_GET['id'] == Auth::user_id()) {
                    header("Location: http://".$server_domain);
                    break;
                }
                $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
                $data = $db->query("SELECT * FROM users WHERE id='".$_GET['id']."'");
                $row = $data->fetch_assoc();
                $user = new User($row['username']);
                $user->follow_toggle();
                header("Location: http://".$server_domain."/page?id=".$user->id);
                break;
            case 'followings_posts' :
                require_once 'views/followings_posts.php';
                break;
            case 'followers' :
                require_once 'views/followers.php';
                break;
            case 'followings' :
                require_once 'views/followings.php';
                break;
            case 'likes' :
                require_once 'views/likes.php';
                break;
            case 'post' :
                require_once 'views/single_post.php';
                break;
                default:
                header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
                break;
        }
    }
}