<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="a place to be seen, a way to share ideas ...">
    <link rel="icon" href="resources/images/turnip_logo.png" type="image/png">
    <title>Turnip | NewPost</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
<?php
use Controllers\Auth\Auth;
use Controllers\val\Validation;
use Models\Post\Post;
use Models\User\User;

require_once 'controllers/Validation.php';
require_once 'Models/Post.php';

$db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
$data = $db->query("SELECT * FROM users WHERE id='".Auth::user_id()."'");
$row = $data->fetch_assoc();
$user = new User($row['username']);
$db->close();

$picture_err = null;
$caption_err = null;

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $post = new Post();

    $picture_err = Validation::picture($_FILES['picture']);

    if($picture_err == null) {
        $post->caption = Validation::test_input($_POST['caption']);
        $post->picture_upload($_FILES['picture']);
        $post->user_id = Auth::user_id();
        $post->save();
        header("Location: http://" . $server_domain . "/page");
    }
}

?>

<?php require_once 'views/header.php'?>

<div style="border: 0;" class="section-top-border">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div align="center" class="single-defination">
                <form action="<?php echo $root; ?>new_post" method="post" enctype="multipart/form-data">
                    <div class="mt-10">
                        <h2>Picture:</h2>
                        <input type="file" name="picture" id="picture" class="single-input" required>
                        <b style="color: red;"><?php echo $picture_err; ?></b>
                    </div>
                    <h2>Caption:</h2>
                    <div class="mt-10">
                        <textarea class="single-textarea" name="caption" maxlength="5000" placeholder="Caption" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Caption'" required></textarea>
                        <b style="color: red;"><?php echo $caption_err; ?></b>
                    </div>
                    <input type="submit" class="genric-btn success" value="Create">
                    <a href="<?php echo $root; ?>" class="genric-btn success">Cancel</a>
                </form>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
<?php require_once 'views/footer.php'?>
</body>
</html>
