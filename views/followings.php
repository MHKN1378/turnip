<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="a place to be seen, a way to share ideas ...">
    <link rel="icon" href="resources/images/turnip_logo.png" type="image/png">
    <title>Turnip | Followers</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
<?php
use Controllers\Auth\Auth;
use Models\Post\Post;
use Models\User\User;

$db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
$data = $db->query("SELECT * FROM users WHERE id='".Auth::user_id()."'");
$row = $data->fetch_assoc();
$user = new User($row['username']);
?>
<?php require_once 'views/header.php'?>

<?php
$db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
$data = $db->query("SELECT * FROM followings WHERE follower_id =".$_GET['id']);
echo "<div style=\"width: 100%; overflow: hidden;\" class=\"section-top-border\">
						<h3 class=\"mb-30 title_color\">Followings</h3>
						<div style=\"width: 95%; overflow: hidden;\" class=\"progress-table-wrap\">
							<div style=\"width: 95%;\" class=\"progress-table\">";
while($row = $data->fetch_assoc())
{
    $daten = $db->query("SELECT * FROM users WHERE id='".$row['user_id']."'");
    $buff = $daten->fetch_assoc();
    $tr_user = new User($buff['username']);
    echo "<div style=\"width: 95%;\" class=\"table-row\">
									<div class=\"country\"><a href=\"".$root."page?id=".$tr_user->id."\"><img style=\"width: 50px; height:50px; border-radius: 25%;\" src=\"".$tr_user->avatar."\" alt=\"flag\"></a></div>
									<div class=\"visit\">".$tr_user->fname." ".$tr_user->lname." (@".$tr_user->username.")</div>
								</div>";
}
echo "</div></div></div>";
$db->close();
?>

<?php require_once 'views/footer.php'?>
</body>
</html>