<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="a place to be seen, a way to share ideas ...">
    <link rel="icon" href="resources/images/turnip_logo.png" type="image/png">
    <title>Turnip | Followings</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
<?php
use Controllers\Auth\Auth;
use Models\Post\Post;
use Models\User\User;

$db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
$data = $db->query("SELECT * FROM users WHERE id='".Auth::user_id()."'");
$row = $data->fetch_assoc();
$user = new User($row['username']);
?>
<?php require_once 'views/header.php'?>

<?php
$db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
$ids = "(";
foreach($user->followings as $val) {
    $ids .= "'".$val."', ";
}
$ids .= "'".$user->id."')";
$data = $db->query("SELECT * FROM posts WHERE user_id IN ".$ids." ORDER BY id DESC");
while($row = $data->fetch_assoc())
{
    $daten = $db->query("SELECT * FROM users WHERE id='".$row['user_id']."'");
    $buff = $daten->fetch_assoc();
    $post_user = new User($buff['username']);
    $this_post = new Post($row['id']);
    echo "<div class=\"row\" id=\"".$row['id']."\"style=\"border: 1px solid gray; margin: 5px; border-radius: 20px;\">
                <div class=\"col-md-3\">";
    echo "<a href=\"".$root."post?id=".$this_post->id."\">";
    echo "<img style=\"margin: 5px;\" src=\"".$row['picture']."\" alt=\"picture\" class=\"img-fluid\">";
    echo "</a>";    echo "</div>
            <div class=\"col-md-9 mt-sm-20 left-align-p\">";
    echo "<a href=\"".$root."page?id=".$post_user->id."\">";
    echo "<img style=\"width: 50px; height:50px; border-radius: 25%; display: inline;\" src=\"".$post_user->avatar."\" alt=\"avatar\"></a>";
    echo "<h4 style=\"display: inline;\">   ".$post_user->fname." ".$post_user->lname."</h4><hr>";
    echo "<p>".$row['caption']."</p>";
    echo "<a href=\"".$root."like?id=".$this_post->id."&re=home"."\">";
    if($user->is_liked($row['id']))
        echo "<i style=\"color: red; font-size: 2em;\" class=\"fa fa-heart\" aria-hidden=\"true\"></i>";
    else
        echo "<i style=\"color: gray; font-size: 2em;\" class=\"fa fa-heart-o\" aria-hidden=\"true\"></i>";
    echo "</a>";
    echo "<a style=\"color: gray;\" href=\"".$root."likes?id=".$this_post->id."\">  ".count($this_post->likes)." Likes";
    echo " "."<i style=\" font-size: 2em;\" class=\"fa fa-comment-o\" aria-hidden=\"true\"></i>";
    echo "  ".count($this_post->comments)." Comments";
    echo "</div>
            </div>";
}
$db->close();
?>

<?php require_once 'views/footer.php'?>
</body>
</html>