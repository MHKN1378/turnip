<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="a place to be seen, a way to share ideas ...">
    <link rel="icon" href="resources/images/turnip_logo.png" type="image/png">
    <title>Turnip | Profile</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
<?php
use Controllers\Auth\Auth;
use Models\User\User;

$db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
$data = $db->query("SELECT * FROM users WHERE id='".Auth::user_id()."'");
$row = $data->fetch_assoc();
$user = new User($row['username']);
$db->close();

?>
<?php require_once 'views/header.php'?>

<?php
if(isset($_GET['id'])) {
    $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
    $data = $db->query("SELECT * FROM users WHERE id='".$_GET['id']."'");
    $row = $data->fetch_assoc();
    $prop_user = new User($row['username']);
    $db->close();
}
else {
    $prop_user = $user;
}
?>

<div style="border: 0;" class="section-top-border">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div align="center" class="single-defination">
                <img align="center" style="width: 50%; height: 50%; border-radius: 25%;" src="<?php echo $prop_user->avatar; ?>" alt="avatar">
                <br><br>
                <h1 class="mb-20">@<?php echo $prop_user->username; ?></h1>
                <br>
                <?php
                if(!empty($prop_user->bio)) {
                    echo "<h2>Bio</h2>";
                    echo "<p>".$prop_user->bio."</p>";
                }
                ?>
                <br>
                <h2>Name</h2>
                <P><?php echo $prop_user->fname." ".$prop_user->lname; ?></P>
                <?php
                if($prop_user->id == $user->id) {
                    echo "<form action=\"".$root."profile\" method=\"post\">
                    <input type=\"hidden\" name=\"key\" value=\"".($prop_user->id * 3 - 2)."\">
                    <input type=\"hidden\" name=\"mode\" value=\"view\">
                    <input type=\"submit\" class=\"genric-btn success\" value=\"Edit\">
                </form>";
                }
                ?>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
<?php require_once 'views/footer.php'?>
</body>
</html>
