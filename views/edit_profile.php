<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="a place to be seen, a way to share ideas ...">
    <link rel="icon" href="resources/images/turnip_logo.png" type="image/png">
    <title>Turnip | EditProfile</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
<?php
use Controllers\Auth\Auth;
use Controllers\val\Validation;
use Models\User\User;

require_once 'controllers/Validation.php';

$db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
$data = $db->query("SELECT * FROM users WHERE id='".Auth::user_id()."'");
$row = $data->fetch_assoc();
$user = new User($row['username']);
$db->close();

$username_err = null;
$fname_err = null;
$lname_err = null;
$bio_err = null;
$avatar_err = null;

if($_POST['mode'] == 'change')
{
    $username_err = Validation::username($_POST['username']);
    $fname_err = Validation::name($_POST['fname']);
    $lname_err = Validation::name($_POST['lname']);
    $avatar_err = Validation::avatar($_FILES['avatar']);

    if($username_err == null && $fname_err == null && $lname_err == null) {
        $user->username = Validation::test_input($_POST['username']);
        $user->fname = Validation::test_input($_POST['fname']);
        $user->lname = Validation::test_input($_POST['lname']);
        $user->bio = Validation::test_input($_POST['bio']);
        $user->avatar_upload($_FILES['avatar']);
        $user->save();
        header("Location: http://" . $server_domain . "/profile");
    }
}

if((($_POST['key'] + 2 ) / 3) != Auth::user_id())
    header("Location: http://".$server_domain."/forbidden");
?>
<?php require_once 'views/header.php'?>

<div style="border: 0;" class="section-top-border">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div align="center" class="single-defination">
                <form action="<?php echo $root; ?>profile" method="post" enctype="multipart/form-data">
                    <img align="center" style="width: 50%; height: 50%; border-radius: 25%;" src="<?php echo $user->avatar; ?>" alt="avatar">
                    <div class="mt-10">
                        <h2>New avatar:</h2>
                        <input type="file" name="avatar" id="avatar" class="single-input" required>
                        <b style="color: red;"><?php echo $avatar_err; ?></b>
                    </div>
                    <h2 class="mb-20">Username:</h2>
                    <div class="input-group-icon mt-10">
                        <div class="icon">@</div>
                        <input type="text" name="username" value="<?php echo isset($_POST['username'])?$_POST['username']:$user->username; ?>" required class="single-input">
                        <b style="color: red;"><?php echo $username_err; ?></b>
                    </div>
                    <h2>Bio:</h2>
                    <div class="mt-10">
                        <textarea class="single-textarea" name="bio" maxlength="100"><?php echo isset($_POST['bio'])?$_POST['bio']:$user->bio; ?></textarea>
                        <b style="color: red;"><?php echo $bio_err; ?></b>
                    </div>
                    <div class="mt-10">
                        <h2>First name:</h2>
                        <input type="text" name="fname" value="<?php echo isset($_POST['fname'])?$_POST['fname']:$user->fname; ?>" required class="single-input">
                        <b style="color: red;"><?php echo $fname_err; ?></b>
                    </div>
                    <div class="mt-10">
                        <h2>Last name:</h2>
                        <input type="text" name="lname" value="<?php echo isset($_POST['lname'])?$_POST['lname']:$user->lname; ?>" class="single-input">
                        <b style="color: red;"><?php echo $lname_err; ?></b>
                    </div>
                    <input type="hidden" name="mode" value="change">
                    <input type="hidden" name="key" value="<?php echo (Auth::user_id() * 3 - 2); ?>">
                    <input type="submit" class="genric-btn success" value="Set">
                </form>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
<?php require_once 'views/footer.php'?>
</body>
</html>
