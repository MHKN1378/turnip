<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="a place to be seen, a way to share ideas ...">
    <link rel="icon" href="resources/images/turnip_logo.png" type="image/png">
    <title>Turnip | Register</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
<?php

require_once 'controllers/Validation.php';
require_once 'controllers/Auth.php';
require_once 'config/settings.php';

use Controllers\Auth\Auth;
use Controllers\val\Validation;
use Models\User\User;

$username_err = null;
$fname_err = null;
$pass_err = null;
$username = null;
$fname = null;
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $username = Validation::test_input($_POST['username']);
    $fname = Validation::test_input($_POST['fname']);

    $username_err = Validation::username($username);
    $fname_err = Validation::name($fname);
    $pass_err = Validation::password($_POST['password'], $_POST['re_password']);

    if($username_err == null && $fname_err == null && $pass_err == null)
    {
        $user = new User($username);
        $user->pass_assign($_POST['password']);
        $user->fname = $fname;
        $user->avatar = "resources/avatars/default.png";
        $user->save();
        Auth::login($username, $_POST['password']);
        header("Location: http://".$server_domain);
    }
}
?>
<div class="section-top-border">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div class="single-defination">
                <h1 class="mb-20" align = "center">Register</h1>
                <form action="<?php echo $root; ?>register" method="post">
                    <div class="mt-10">
                        <input type="text" name="username" placeholder="Username" value=
                        "<?php if(isset($_POST['username'])) echo $username; ?>"
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Username'" required class="single-input">
                        <b style="color: red;"><?php echo $username_err; ?></b>
                    </div>
                    <div class="mt-10">
                        <input type="text" name="fname" placeholder="Name" value=
                        "<?php if(isset($_POST['fname'])) echo $fname; ?>"
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name'" required class="single-input">
                        <b style="color: red;"><?php echo $fname_err; ?></b>
                    </div>
                    <div class="mt-10">
                        <input type="password" name="password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" required class="single-input">
                        <b style="color: red;"><?php echo $pass_err; ?></b>
                    </div>
                    <div class="mt-10">
                        <input type="password" name="re_password" placeholder="Re-Enter Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Re-Enter Password'" required class="single-input">
                    </div>
                    <div align="center" class="mt-10">
                        <input type="submit" class="genric-btn success" value="Register">
                        <a href="<?php echo $root; ?>login" class="genric-btn success">Login</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
</body>
</html>