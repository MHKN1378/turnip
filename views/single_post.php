<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="a place to be seen, a way to share ideas ...">
    <link rel="icon" href="resources/images/turnip_logo.png" type="image/png">
    <title>Turnip | Post</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
<?php
use Controllers\Auth\Auth;
use Controllers\val\Validation;
use Models\Post\Post;
use Models\User\User;

require_once 'controllers/Validation.php';

$db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
$data = $db->query("SELECT * FROM users WHERE id='".Auth::user_id()."'");
$row = $data->fetch_assoc();
$user = new User($row['username']);
$db->close();

?>
<?php require_once 'views/header.php'?>

<?php
if (isset($_GET['id'])) {
    $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
    $data = $db->query("SELECT * FROM posts WHERE id='" . $_GET['id'] . "'");
    $row = $data->fetch_assoc();
    $post = new Post($row['id']);
    $data = $db->query("SELECT * FROM users WHERE id='" . $post->user_id . "'");
    $row = $data->fetch_assoc();
    $post_user = new User($row['username']);
    $db->close();
} else {
    header("Location : http://" . $server_domain);
}

?>

<div style="border: 0;" class="section-top-border">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div align="center" class="single-defination">
                <img align="center" style="width: 95%; height: auto;" src="<?php echo $post->picture; ?>" alt="picture">
                <div class="col-md-9 mt-sm-20 left-align-p">
                <a href="<?php echo $root."page?id=".$post_user->id ?>">
                    <img style="width: 50px; height:50px; border-radius: 25%; display: inline;" src="<?php echo $post_user->avatar; ?>" alt="avatar">
                </a>
                <h4 style="display: inline;"><?php echo $post_user->fname." ".$post_user->lname; ?></h4><hr>
                <p><?php echo $post->caption; ?></p>
                <a href="<?php echo $root."like?id=".$post->id."&re=post"; ?>">
                    <?php
                    if($user->is_liked($post->id))
                    echo "<i style=\"color: red; font-size: 2em;\" class=\"fa fa-heart\" aria-hidden=\"true\"></i>";
                    else
                    echo "<i style=\"color: gray; font-size: 2em;\" class=\"fa fa-heart-o\" aria-hidden=\"true\"></i>";
                    ?>
                </a>
                <?php echo "  ".count($post->likes)." Likes"; ?>
                <i style=" font-size: 2em;" class="fa fa-comment-o" aria-hidden="true"></i>
                <?php echo "  ".count($post->comments)." Comments"; ?>
                </div>
                <hr>
                <?php
                $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
                $data = $db->query("SELECT * FROM comments WHERE post_id=".$post->id." ORDER BY c_timestamp DESC");
                while($row = $data->fetch_assoc())
                {
                    $daten = $db->query("SELECT * FROM users WHERE id=".$row['user_id']);
                    $buff = $daten->fetch_assoc();
                    $comment_user = new User($buff['username']);

                    echo "<div class=\"row\" style=\"width: 90%;\">
                    <div class=\"col-lg-12\">
                        <blockquote class=\"generic-blockquote\">";
                echo "<a href=\"".$root."page?id=".$comment_user->id."\">
                    <img style=\"width: 50px; height:50px; border-radius: 25%; display: inline;\" src=\"".$comment_user->avatar."\" alt=\"avatar\">
                </a>
                <h4 style=\"display: inline;\">".$comment_user->fname." ".$comment_user->lname."</h4><br><hr>";
                    echo $row['text'];
                    echo "</blockquote>
                    </div>
                </div>";
                }
                $db->close();
                ?>
                <form action="<?php echo $root; ?>comment" method="post">
                    <div class="mt-10">
                        <textarea class="single-textarea" name="text" maxlength="200" placeholder="add a comment" onfocus="this.placeholder = ''" onblur="this.placeholder = 'add a comment'" required></textarea>
                    </div>
                    <input type="hidden" value="<?php echo $post->id; ?>" name="post_id">
                    <input type="submit" class="genric-btn success" value="Send">
                </form>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
<?php require_once 'views/footer.php'?>
</body>
</html>
