<?php require_once 'config/settings.php'?>
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container box_1620">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="<?php echo $root; ?>"><img style="width: 50px; height:50px " src="resources/images/turnip_logo.png" alt=""><h1 style="display: inline;"> Turnip™</h1></a>
                <a href="<?php echo $root; ?>page"></a>
                <a href="<?php echo $root; ?>profile" align="right"class="navbar-brand logo_h" alt="avatar"><img style="width: 50px; height:50px; border-radius: 25%;" src="<?php echo $user->avatar; ?>" alt="no"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav">
                        <li class="nav-item"><a class="nav-link" href="<?php echo $root; ?>followings_posts">My Followings</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo $root; ?>page">My Page</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo $root; ?>new_post">New Post</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo $root; ?>logout">Logout</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right header_social ml-auto">
                        <li class="nav-item"><a class="logo_h" href="<?php echo $root; ?>"><img style="width: 50px; height:50px " src="resources/images/turnip_logo.png" alt=""><h1 style="display: inline; color:black;"> Turnip™</h1></a></li>
                        <li class="nav-item submenu dropdown active">
                            <a href="profile" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $user->fname." ".$user->lname; ?></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item"><a class="nav-link" href="<?php echo $root; ?>profile">Profile</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?php echo $root; ?>page">Page</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?php echo $root; ?>logout">Logout</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><img style="width: 50px; height:50px; border-radius: 25%;" src="<?php echo $user->avatar; ?>" alt="avatar"></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<br>
<br>
<br>