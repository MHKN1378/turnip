<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="a place to be seen, a way to share ideas ...">
    <link rel="icon" href="resources/images/turnip_logo.png" type="image/png">
    <title>Turnip | Login</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
<?php

use Controllers\Auth\Auth;
use Models\User\User;
require_once 'config/settings.php';

$username_err = null;
$pass_err = null;

if($_SERVER['REQUEST_METHOD'] == 'POST') {

    $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
    $data = $db->query("SELECT * FROM users WHERE username='" . $_POST['username'] . "'");
    if ($data->num_rows == 0)
        $username_err = "This username does not exist";
    else {
        $user = new User($_POST['username']);
        if (!$user->pass_check($_POST['password']))
            $pass_err = "wrong password";
    }
    if ($username_err == null && $pass_err == null) {
        Auth::login($_POST['username'], $_POST['password']);
        header("Location: http://".$server_domain);
    }
}
?>
<div class="section-top-border">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div class="single-defination">
                <h1 class="mb-20" align = "center">Login</h1>
                <form action="<?php echo $root; ?>login" method="post">
                    <div class="mt-10">
                        <input type="text" name="username" placeholder="Username" value=
                        "<?php if(isset($_POST['username'])) echo $_POST['username']; ?>" onfocus="
                         this.placeholder = ''" onblur="this.placeholder = 'Username'" required class="single-input">
                        <b style="color: red;"><?php echo $username_err; ?></b>
                    </div>
                    <div class="mt-10">
                        <input type="password" name="password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" required class="single-input">
                        <b style="color: red;"><?php echo $pass_err; ?></b>
                    </div>
                    <div align="center" class="mt-10">
                        <input type="submit" class="genric-btn success" value="Login">
                        <a href="<?php echo $root; ?>register" class="genric-btn success">Register</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
</body>
</html>