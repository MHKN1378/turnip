<?php namespace Controllers\Auth;

require_once ('Models/User.php');

use Models\User\User;

class Auth
{
    static function logged_in () {
        return isset($_SESSION["current_user"]);
    }

    static function user_id () {
        if(Auth::logged_in())
            return $_SESSION["current_user"];
        else
            return false;
    }

    static function login ($username, $password) {
        $current = new User($username);
        if($current->error)
            return $current->error;
        if(!$current->pass_check($password))
            return "wrong password";
        $_SESSION["current_user"] = $current->id;
        return null;
    }

    static function logout () {
        session_unset();
    }
}