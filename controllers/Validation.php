<?php namespace Controllers\val;

use Controllers\Auth\Auth;
use mysqli;

require_once 'config/settings.php';

class Validation
{
    static function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        $data = addslashes($data);
        return $data;
    }

    static function username($input) {
        if (!preg_match("/^[a-zA-Z0-9]*$/",$input))
            return "Only letters and numbers allowed";
        $db = new mysqli($GLOBALS['db_server'], $GLOBALS['db_username'], $GLOBALS['db_password'], $GLOBALS['db_name']);
        $data = $db->query("SELECT * FROM users WHERE username='".$input."'");
        if(!Auth::logged_in() && $data->num_rows != 0)
        {
            return "this username already exists";
        }
        if(Auth::logged_in() && $data->num_rows != 0)
        {
            $row = $data->fetch_assoc();
            if(Auth::user_id() != $row['id'])
                return "this username already exists";
        }
        return null;
    }

    static function name($input) {
        if (!preg_match("/^[a-zA-Z ]*$/",$input))
            return "Only letters and white space allowed";
        return null;
    }

    static function password($input1, $input2) {
        if ($input1 !== $input2)
            return "passwords not match";
        if (!preg_match("/^[a-zA-Z0-9]*$/",$input1))
            return "Only letters and numbers allowed";
        return null;
    }

    static function avatar($file) {
        $target_dir = "resources/avatars/";
        $target_file = $target_dir . basename($file["name"]);
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($file["tmp_name"]);
            if($check === false){
                return "File is not an image";
            }
        }
        if(empty(basename($file["name"])))
        {
            return null;
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            return "Sorry, file already exists";
        }
        // Check file size
        if ($file["size"] > 500000) {
            return "Sorry, your file is too large";
        }
        // Allow certain file formats
        if($imageFileType != "png") {
            return "Sorry, only PNG file is allowed";
        }
        return null;
    }

    static function picture($file) {
        $target_dir = "resources/posts/";
        $target_file = $target_dir . basename($file["name"]);
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($file["tmp_name"]);
            if($check === false){
                return "File is not an image";
            }
        }
        if(empty(basename($file["name"])))
        {
            return null;
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            return "Sorry, file already exists";
        }
        // Check file size
        if ($file["size"] > 10000000) {
            return "Sorry, your file is too large";
        }
        // Allow certain file formats
        if($imageFileType != "png" && $imageFileType != "jpg") {
            return "Sorry, only PNG and JPG files are allowed";
        }
        return null;
    }
}